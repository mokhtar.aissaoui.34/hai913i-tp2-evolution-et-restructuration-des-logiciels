package AstParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import Tp.ClassVisitor;
import Tp.Comparateur;
import Tp.PackageVisitor;
import Tp.VariableDeclarationFragmentVisitor;
import Tp.MethodDeclarationVisitor;

public class Parser {
 
 public static final String projectPath = "C:\\Users\\mokhtar\\eclipse-workspace\\HAI913-TP3";
 public static final String projectSourcePath = projectPath + "\\src";
 public static final String jrePath = "C:\\Program Files\\Java\\jre1.8.0_291\\lib\\rt.jar";
 public static HashMap<String, Integer> nbrOfMethodsInClasses = new HashMap<String, Integer>();
 public static HashMap<String, Integer> nbrOfMethodsInClasses2 = new HashMap<String, Integer>();
 public static HashMap<String, Integer> nbrOfattributsInClasses = new HashMap<String, Integer>();
 



 public static void main(String[] args) throws IOException {
  
  System.out.println("Question 1-le nombre de classes :"+getCountClasses());
  System.out.println("Question 2 - leNombre de lignes"+getCountLignes());
  System.out.println("Question 3- le nombre de methodes"+getCountMethode());
  System.out.println("Question 4- le nombre de package"+getCountPackages());
  System.out.println("Qestion 5- le nombre moyen de methode par classe"+getNbMthdInClass());
  System.out.println("Question 6- le nombre moyen de ligne par methode"+nbLineParMeth());
  System.out.println("Question 7- le nombre moyen d'attributs par class"+avgAttrPerClass());
  System.out.println("Question 8- Les 10% des classes qui poss�dent le plus grand nombre de m�thodes"+ getFirstDecileOfClassesMethodsNumber());
  System.out.println("Question 9- Les 10% des classes qui poss�dent le plus grand nombre d�attributs"+getFirstDecileOfClassesAttributeNumber());
  System.out.println("Question 10- Les classes qui font partie en m�me temps des deux cat�gories pr�c�dentes"+classesWhichBelongsAttributeAndMethode());
  System.out.println("Question 11- Les classes qui poss�dent plus de X m�thodes (la valeur de X est donn�e). "+getClassesWithXMethodsNumber(4));
  System.out.println("Question 12- Les 10% des m�thodes qui poss�dent le plus grand nombre de lignes de code "
    +getMethodsWithGreatestLineCodeNbrByClass());
  System.out.println("Question 13- Le nombre maximal de param�tres par rapport � toutes les m�thodes de"
    + " l�application "+getNmbMaxDePar());

 }
 

 // read all java files from specific folder
 public static ArrayList<File> listJavaFilesForFolder(final File folder) {
  ArrayList<File> javaFiles = new ArrayList<File>();
  for (File fileEntry : folder.listFiles()) {
   if (fileEntry.isDirectory()) {
    javaFiles.addAll(listJavaFilesForFolder(fileEntry));
   } else if (fileEntry.getName().contains(".java")) {
    javaFiles.add(fileEntry);
   }
  }

  return javaFiles;
 }
 
 //create AST
 private static CompilationUnit parse(char[] classSource) {
  ASTParser parser = ASTParser.newParser(AST.JLS4); // java +1.6
  parser.setResolveBindings(true);
  parser.setKind(ASTParser.K_COMPILATION_UNIT);
 
  parser.setBindingsRecovery(true);
 
  Map options = JavaCore.getOptions();
  parser.setCompilerOptions(options);
 
  parser.setUnitName("");
 
  String[] sources = { projectSourcePath }; 
  String[] classpath = {jrePath};
 
  parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
  parser.setSource(classSource);
  
  return (CompilationUnit) parser.createAST(null); // create and parse
   }
 public static int getCountClasses() throws IOException {
 int nbClass=0;
  final File folder = new File(projectSourcePath);
  ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
  for (File fileEntry : javaFiles) {
   String content = FileUtils.readFileToString(fileEntry);
   ClassVisitor c1 = new ClassVisitor();
   CompilationUnit parse = parse(content.toCharArray());
   parse.accept(c1);
  nbClass+= c1.getcountclass();
  }
  return nbClass;}
 public static int getCountLignes() throws IOException {
  int count = 0;
   final File folder = new File(projectSourcePath);
   ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
   for (File fileEntry : javaFiles) {
    String content = FileUtils.readFileToString(fileEntry);
    CompilationUnit parse = parse(content.toCharArray());
    String code= parse.toString();
    int lastIndex = 0;
    

    while(lastIndex != -1){

        lastIndex = code.indexOf("\n",lastIndex);

        if(lastIndex != -1){
            count ++;
            lastIndex += "\n".length();
        }
    }
    
   }
   return count;}
 public static int getCountMethode() throws IOException {
  int nbClass=0;
  final File folder = new File(projectSourcePath);
  ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
  for (File fileEntry : javaFiles) {
   String content = FileUtils.readFileToString(fileEntry);
   MethodDeclarationVisitor c1 = new MethodDeclarationVisitor();
   CompilationUnit parse = parse(content.toCharArray());
   parse.accept(c1);
  nbClass+= c1.getMethods().size();
  }
  return nbClass;}
 public static int getCountPackages() throws IOException {
  int nbClass=0;
   final File folder = new File(projectSourcePath);
   ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
   for (File fileEntry : javaFiles) {
    String content = FileUtils.readFileToString(fileEntry);
    PackageVisitor c1 = new PackageVisitor();
    CompilationUnit parse = parse(content.toCharArray());
    parse.accept(c1);
   nbClass+= c1.getNbPackages();
   }
   return nbClass;}

 public static double getNbMthdInClass() throws IOException {
  return getCountMethode()/getCountClasses();}
 
 public static int getCountLigneMethode() throws IOException {
  int count=0;
  final File folder = new File(projectSourcePath);
  ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
  for (File fileEntry : javaFiles) {
   String content = FileUtils.readFileToString(fileEntry);
   MethodDeclarationVisitor c1 = new MethodDeclarationVisitor();
   CompilationUnit parse = parse(content.toCharArray());
   parse.accept(c1);
  for(int i=0;i<c1.getMethods().size();i++){
  String code = c1.getMethods().get(i).toString();
int lastIndex = 0;
  

  while(lastIndex != -1){

      lastIndex = code.indexOf("\n",lastIndex);

      if(lastIndex != -1){
          count ++;
          lastIndex += "\n".length();
      }
  }
  }
  
  
 }
 return count;}

public static double nbLineParMeth() throws IOException {
 return getCountLigneMethode()/getCountMethode();
}
public static double avgAttrPerClass() throws IOException {
 int count=0;
 final File folder = new File(projectSourcePath);
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  VariableDeclarationFragmentVisitor c1 = new VariableDeclarationFragmentVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  count += c1.getVariables().size();
 
 }
 return count/getCountClasses();
 }
public static HashMap<String, Integer> getFirstDecileOfClassesMethodsNumber() throws IOException {
 final File folder = new File(projectSourcePath);
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  ClassVisitor c1 = new ClassVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  c1.getClasses()
  .stream().forEach(s -> nbrOfMethodsInClasses.put(s.getName().toString(), s.getMethods().length));
 }
 int size = nbrOfMethodsInClasses.size()*10/100+1;
  Comparateur comp =  new Comparateur(nbrOfMethodsInClasses);
     TreeMap<String, Integer> nbrOfMethodsInClasses1 = new TreeMap<String, Integer>(comp);
     nbrOfMethodsInClasses1.putAll(nbrOfMethodsInClasses);
     Set<Entry<String, Integer>> entrySet = nbrOfMethodsInClasses1.entrySet();

      // Convert entrySet to Array using toArray method
      Map.Entry<String, Integer>[] entryArray  = entrySet.toArray( new Map.Entry[entrySet.size()]);
      HashMap<String, Integer> resu = new HashMap<String, Integer>();

      for(int i=0;i<size;i++) {
       resu.put(entryArray[i].getKey(),entryArray[i].getValue());
       
      }
      return resu;


}
public static HashMap<String, Integer> getFirstDecileOfClassesAttributeNumber() throws IOException {
 final File folder = new File(projectSourcePath);
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  ClassVisitor c1 = new ClassVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  c1.getClasses()
  .stream().forEach(s -> nbrOfattributsInClasses.put(s.getName().toString(), s.getFields().length));
 }
 int size = nbrOfattributsInClasses.size()*10/100+1;
  Comparateur comp =  new Comparateur(nbrOfMethodsInClasses);
     TreeMap<String, Integer> nbrOfattributsInClasses1 = new TreeMap<String, Integer>(comp);
     nbrOfattributsInClasses1.putAll(nbrOfattributsInClasses);
     Set<Entry<String, Integer>> entrySet = nbrOfattributsInClasses1.entrySet();

      // Convert entrySet to Array using toArray method
      Map.Entry<String, Integer>[] entryArray  = entrySet.toArray( new Map.Entry[entrySet.size()]);
      HashMap<String, Integer> resu = new HashMap<String, Integer>();

      for(int i=0;i<size;i++) {
       resu.put(entryArray[i].getKey(),entryArray[i].getValue());
       
      }
      return resu;


}
public static ArrayList<String> classesWhichBelongsAttributeAndMethode() throws IOException {
 HashMap<String, Integer> c1 = getFirstDecileOfClassesMethodsNumber();
 HashMap<String, Integer> c2 = getFirstDecileOfClassesAttributeNumber();
 ArrayList<String> t = new ArrayList<String>();
 c1.forEach((x,y)->{if(c2.get(x)!=null) t.add(x);  });
 return t;

}

public static HashMap<String, Integer> getClassesWithXMethodsNumber(int x) throws IOException {

 final File folder = new File(projectSourcePath);
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  ClassVisitor c1 = new ClassVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  c1.getClasses()
  .stream().forEach(s -> {if(s.getMethods().length>x) nbrOfMethodsInClasses2.put(s.getName().toString(), s.getMethods().length);});
 }
 
      
       
      return nbrOfMethodsInClasses2;

}


public static HashMap<String, Integer> getMethodsWithGreatestLineCodeNbrByClass() throws IOException {
 HashMap<String, Integer> map = new HashMap<String, Integer>();
 final File folder = new File(projectSourcePath);
 
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  MethodDeclarationVisitor c1 = new MethodDeclarationVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  c1.getMethods()
  .stream().forEach(s -> {
   int count = 0;
   String code = s.toString();
   int lastIndex = 0;
   

   while(lastIndex != -1){

       lastIndex = code.indexOf("\n",lastIndex);

       if(lastIndex != -1){
           count ++;
           lastIndex += "\n".length();
       }
   }
   map.put(s.getName().toString(), count);
   
  });
 }
 
 int size = map.size()*10/100+1;
  Set<Entry<String, Integer>> entrySet = map.entrySet();

   // Convert entrySet to Array using toArray method
   Map.Entry<String, Integer>[] entryArray  = entrySet.toArray( new Map.Entry[entrySet.size()]);
   HashMap<String, Integer> resu = new HashMap<String, Integer>();

   for(int i=0;i<size;i++) {
    resu.put(entryArray[i].getKey(),entryArray[i].getValue());
    
   }
   return resu;

}
public static HashMap<String, Integer> getNmbMaxDePar() throws IOException {
 HashMap<String, Integer> map = new HashMap<String, Integer>();
 final File folder = new File(projectSourcePath);
 
 ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
 for (File fileEntry : javaFiles) {
  String content = FileUtils.readFileToString(fileEntry);
  MethodDeclarationVisitor c1 = new MethodDeclarationVisitor();
  CompilationUnit parse = parse(content.toCharArray());
  parse.accept(c1);
  c1.getMethods()
  .stream().forEach(s -> {
   map.put(s.getName().toString(), s.parameters().size());
   
  });
 }
 
 
  Set<Entry<String, Integer>> entrySet = map.entrySet();

   // Convert entrySet to Array using toArray method
   Map.Entry<String, Integer>[] entryArray  = entrySet.toArray( new Map.Entry[entrySet.size()]);
   HashMap<String, Integer> resu = new HashMap<String, Integer>();

   
    resu.put(entryArray[0].getKey(),entryArray[0].getValue());
    
   
   return resu;

}

}

