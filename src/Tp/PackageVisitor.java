package Tp;

import java.util.*;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.PackageDeclaration;

public class PackageVisitor  extends ASTVisitor{
	private List<PackageDeclaration> packages = new ArrayList<>();
	
	public boolean visit(PackageDeclaration node) {
		packages.add(node);
		
		return super.visit(node);
	}
	public PackageVisitor() {
		// TODO Auto-generated constructor stub
	}
	public int getNbPackages() {
		return packages.size();
	}
	

}
