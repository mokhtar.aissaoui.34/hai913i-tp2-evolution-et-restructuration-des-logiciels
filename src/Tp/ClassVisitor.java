package Tp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class ClassVisitor extends ASTVisitor{
	
	private ArrayList<TypeDeclaration> classs = new ArrayList<>();


	public boolean visit(TypeDeclaration node) {
		if(!node.isInterface())
			classs.add(node);
		return super.visit(node);
	}
	
	public int getcountclass() {
		return this.classs.size();
	}

	public ArrayList<TypeDeclaration> getClasses() {
		return classs;
	}

}
